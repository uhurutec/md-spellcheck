#!/bin/bash
#
# Copyright (C) 2022-2024 UhuruTec AG <hello@uhurutec.com>
# SPDX-License-Identifier: AGPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify it in
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
# details.
#
# You should have received a copy of the GNU Affero General Public License along
# with this program. If not, see <https://www.gnu.org/licenses/>.

shopt -s -o errexit
shopt -s -o errtrace
shopt -s -o functrace
shopt -s -o nounset
shopt -s -o pipefail
shopt -s dotglob
shopt -s globstar
shopt -s gnu_errfmt
shopt -s inherit_errexit
shopt -s lastpipe
shopt -s nullglob

this="$(basename "$0")"

# process command line arguments
function process_args {
  directory=''
  dictionary=''
  excludes=()
  local count=0
  while (("$#")); do
    case "$1" in
      "-h" | "--help")
        usage
        exit 0
        ;;
      "-d" | "--dictionary")
        dictionary="${2:-}"
        shift
        ;;
      "-e" | "--exclude")
        excludes+=("${2:-}")
        shift
        ;;
      *)
        case $count in
          0)
            directory="$1"
            ;;
          *)
            usage
            exit 1
            ;;
        esac
        ((count = count + 1))
        ;;
    esac
    shift
  done
}

function usage_print_option_switch {
  local short="$1"
  local long="$2"
  local description="$3"
  printf " -%s | --%s\t%s\n" "$short" "$long" "$description"
}

function usage_print_optional_parameter {
  local short="$1"
  local long="$2"
  local variable="$3"
  local description="$4"
  printf " -%s | --%s %s\t%s\n" "$short" "$long" "$variable" "$description"
}

function usage {
  printf "Usage: %s [OPTIONS] DIRECTORY\n" "$this"
  printf "Spellcheck all .md files in DIRECTORY\n"
  printf "\nOptions:\n"
  usage_print_option_switch "h" "help" "display this help message"
  usage_print_optional_parameter "d" "dictionary" "DICTIONARY" "file with additional list of words"
  usage_print_optional_parameter "e" "exclude" "PATH" "exclude this path from spellchecking (can be given multiple times)"
}

# Generate aspell dictionary from word list.
# The aspell dictionary format is just a list of words, one per line, prepended
# by a header in following form:
#    personal_ws-1.1 lang num [encoding]
# It is easier to maintain a plain list of files as dictionary, therefore we
# generate the dictionary on the fly.
function aspell_dict_from_list {
  local dictionary_content=("$@")
  local word_list=()

  # Ignore lines, which contain only white space.
  for word in "${dictionary_content[@]}"; do
    if [ -n "${word/ /}" ]; then
      word_list+=("$word")
    fi
  done

  printf "personal_ws-1.1 en %d\n" "${#word_list[@]}"
  if [ "${#word_list[@]}" != 0 ]; then
    printf "%s\n" "${word_list[@]}"
  fi
}

# Make sure there is only one word per line in the dictionary.
function check_dictionary {
  local word_list=("$@")
  for word in "${word_list[@]}"; do
    if [[ $word =~ [[:space:]]+ ]]; then
      echo "Dictionary can have only one word per line." >&2
      return 1
    fi
  done
}

function is_excluded {
  local file="$1"
  for exclude in "${excludes[@]}"; do
    if [[ "$file" =~ ^"$directory/$exclude"* ]]; then
      return 0
    fi
  done
  return 1
}

function main {
  process_args "$@"

  # remove trailing slash, if any
  directory="${directory%/}"

  if [ ! -d "$directory" ]; then
    usage
    exit 1
  fi
  if [ -z "$dictionary" ]; then
    dictionary="$directory/dictionary.txt"
  fi
  if ! hash aspell 2>/dev/null; then
    echo "Need aspell for spellchecking"
    exit 1
  fi

  dictionary_content=()
  if [ -f "$dictionary" ]; then
    readarray -t dictionary_content <"$dictionary"
  fi

  check_dictionary "${dictionary_content[@]}"

  local status="0"
  local word_list
  for file in "$directory"/**/*.md; do
    if ! is_excluded "$file"; then
      aspell --personal=<(aspell_dict_from_list "${dictionary_content[@]}") \
        --mode=markdown --lang=en --encoding=utf-8 list <"$file" \
        | readarray -t word_list
      if [ "${#word_list[@]}" != 0 ]; then
        echo "Misspelled word(s) in \"$file\":" >&2
        printf " %s\n" "${word_list[@]}" | sort -u >&2
        status="1"
      fi
    fi
  done
  return "$status"
}

main "$@"
