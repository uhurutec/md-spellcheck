# md-spellcheck

Simple spellcheck script for Markdown files using `aspell`.

## Usage

```sh
spellcheck --dictionary /some/dictionary_file.txt /some/directory/
```

## Description

This script uses aspell, which can already spellcheck Markdown files, but has no
distinct batch mode for non-interactive checks of a lot of files. It has,
however, a mode to list all miss-spelled words of a file. The script just checks
if that list is empty for all files with extension `.md`.

## Licence

This project is licensed under the *GNU Affero General Public License 3* - see
the [LICENSE](LICENSE) file for details

Copyright (C) 2022-2024 UhuruTec AG <hello@uhurutec.com>
