#!/bin/bash
#
# Copyright (C) 2022-2024 UhuruTec AG <hello@uhurutec.com>
# SPDX-License-Identifier: AGPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify it in
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
# details.
#
# You should have received a copy of the GNU Affero General Public License along
# with this program. If not, see <https://www.gnu.org/licenses/>.

shopt -s -o errexit
shopt -s -o errtrace
shopt -s -o functrace
shopt -s -o nounset
shopt -s -o pipefail
shopt -s dotglob
shopt -s globstar
shopt -s gnu_errfmt
shopt -s inherit_errexit
shopt -s lastpipe
shopt -s nullglob

this="$(basename "$0")"
command_dependencies=(buildah)
variables=(
  CI_REGISTRY_PASSWORD
  CI_REGISTRY_USER
  CI_REGISTRY
  CI_REGISTRY_IMAGE
)

# process command line arguments
function process_args {
  tag=''
  latest=''
  local count=0
  while (("$#")); do
    case "$1" in
      "-h" | "--help")
        usage
        exit 0
        ;;
      "-l" | "--latest")
        latest='yes'
        ;;
      *)
        case $count in
          0)
            tag="$1"
            ;;
          *)
            usage
            exit 1
            ;;
        esac
        ((count = count + 1))
        ;;
    esac
    shift
  done
}

function usage_print_option_switch {
  local short="$1"
  local long="$2"
  local description="$3"
  printf " -%s, --%s\t%s\n" "$short" "$long" "$description"
}

function usage {
  printf "Usage: %s [OPTIONS] TAG\n" "$this"
  printf "Create a container with spellcheck script and tag with TAG if given\n"
  printf "\nOptions:\n"
  usage_print_option_switch "h" "help" "display this help message"
  usage_print_option_switch "l" "latest" "tag this image also \"latest\""
}

function check_commands {
  for command_dependency in "${command_dependencies[@]}"; do
    if ! hash "$command_dependency" 2>/dev/null; then
      echo "Command \"$command_dependency\" not available!" >&2
      usage
      exit 1
    fi
  done
}

function check_environment {
  local status='0'
  for env_var in "${variables[@]}"; do
    if ! [[ -v $env_var ]]; then
      echo "$env_var is not set" >&2
      status='1'
    fi
  done
  return "$status"
}

function main {
  process_args "$@"
  check_commands
  check_environment
  if [ -z "$tag" ]; then
    echo "Please, specify a tag!" >&2
    usage
    exit 1
  fi
  echo "$CI_REGISTRY_PASSWORD" | buildah login -u "$CI_REGISTRY_USER" --password-stdin "$CI_REGISTRY"
  local image="$CI_REGISTRY_IMAGE:$tag"
  buildah build -f Dockerfile -t "$image" .
  working_container=$(buildah from "$image")
  buildah commit --squash "$working_container" "$image"
  buildah push "$image"
  if [ -n "$latest" ]; then
    local latest_image="$CI_REGISTRY_IMAGE:latest"
    buildah tag "$image" "$latest_image"
    buildah push "$latest_image"
  fi
}

main "$@"
